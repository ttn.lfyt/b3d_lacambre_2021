name: whistle.20210923173155.D50.L20.W1.2.T1.5.A45.O10.R2
timestamp: 2021/09/23 17:31:55
inner diameter: 50cm
outer diameter: 52.4cm
total length: 20cm
chamber length: 10cm
chamber volume: 1570.7963267948967cm3
wall thickness: 1.2cm
intake position: 10cm
intake angle: 45°
reducer offset: 2cm
cleanup: True
